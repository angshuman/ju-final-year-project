from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import json
from configurations import *
from authentication import *


class listener(StreamListener):

    def on_data(self, data):
        all_data = json.loads(data)
        opfile.write(str(all_data)+'\n')
        return(True)

    def on_error(self, status):
        return

def main():
    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)
    auth.secure=False 
    while True:
        try:        
            twitterStream = Stream(auth, listener())
            twitterStream.filter(track=keys)
        except KeyboardInterrupt:
			opfile.close()
			raise
        except:
            pass
if __name__=='__main__':
    main()
