from tweepy import Stream
from tweepy import OAuthHandler
from tweepy import API 
from tweepy.streaming import StreamListener
import json
from configurations import *
from authentication import *
import time
last_updated=0
countries={"Worldwide":1,'Canada': 23424775, 'BE': 23424757, 'JP': 23424856, 'JO': 23424860, 'BR': 23424768, 'Argentina': 23424747, 'BY': 23424765, 'Ghana': 23424824, 'RU': 23424936, 'Saudi Arabia': 23424938, 'Guatemala': 23424834, 'Jordan': 23424860, 'Spain': 23424950, 'GT': 23424834, 'GR': 23424833, 'BH': 23424753, 'Vietnam': 23424984, 'GB': 23424975, 'New Zealand': 23424916, 'PH': 23424934, 'GH': 23424824, 'Pakistan': 23424922, 'OM': 23424898, 'United Arab Emirates': 23424738, 'India': 23424848, 'Kenya': 23424863, 'Turkey': 23424969, 'France': 23424819, 'Peru': 23424919, 'Norway': 23424910, 'PR': 23424935, 'Korea': 23424868, 'PT': 23424925, 'Singapore': 23424948, 'PA': 23424924, 'PE': 23424919, 'PK': 23424922, 'Dominican Republic': 23424800, 'PL': 23424923, 'Ukraine': 23424976, 'Bahrain': 23424753, 'None': 1, 'EG': 23424802, 'Indonesia': 23424846, 'ZA': 23424942, 'EC': 23424801, 'United States': 23424977, 'Sweden': 23424954, 'Belarus': 23424765, 'Russia': 23424936, 'ES': 23424950, 'Portugal': 23424925, 'South Africa': 23424942, 'US': 23424977, 'Malaysia': 23424901, 'Austria': 23424750, 'Latvia': 23424874, 'Japan': 23424856, 'MY': 23424901, 'MX': 23424900, 'AT': 23424750, 'Brazil': 23424768, 'FR': 23424819, 'Kuwait': 23424870, 'Panama': 23424924, 'Ireland': 23424803, 'Nigeria': 23424908, 'Ecuador': 23424801, 'Australia': 23424748, 'NL': 23424909, 'Algeria': 23424740, 'NO': 23424910, 'NG': 23424908, 'NZ': 23424916, 'Chile': 23424782, 'Puerto Rico': 23424935, 'Belgium': 23424757, 'Thailand': 23424960, 'Philippines': 23424934, 'CH': 23424957, 'CO': 23424787, 'CL': 23424782, 'Denmark': 23424796, 'CA': 23424775, 'Switzerland': 23424957, 'KE': 23424863, 'Lebanon': 23424873, 'KR': 23424868, 'KW': 23424870, 'Colombia': 23424787, 'SA': 23424938, 'SG': 23424948, 'SE': 23424954, 'DO': 23424800, 'Qatar': 23424930, 'Italy': 23424853, 'DK': 23424796, 'DE': 23424829, 'Netherlands': 23424909, 'DZ': 23424740, 'Venezuela': 23424982, 'United Kingdom': 23424975, 'Israel': 23424852, 'LB': 23424873, 'TR': 23424969, 'LV': 23424874, 'Germany': 23424829, 'TH': 23424960, 'Oman': 23424898, 'Poland': 23424923, 'AE': 23424738, 'VE': 23424982, 'IT': 23424853, 'VN': 23424984, 'AR': 23424747, 'AU': 23424748, 'IL': 23424852, 'IN': 23424848, 'IE': 23424803, 'ID': 23424846, 'Mexico': 23424900, 'Egypt': 23424802, 'UA': 23424976, 'QA': 23424930, 'Greece': 23424833}
def update_keywords(keywords,tracking,api):
	global last_updated
	if abs(last_updated-time.time())<3600:
		return (keywords)
	last_updated=time.time()
	for place in tracking:
		print (place)
		woeid=countries[place]
		trends=None
		try:
			trends=api.trends_place(woeid)[0]
			trends=trends['trends']
			for trend in trends:
				keywords.append(trend['query'])
		except KeyboardInterrupt:
			raise
		except:
			pass
	keywords=(set(keywords))
	for key in keywords:
		print (key)
	keywords=list(keywords)
	keywords_file.write("\n"+json.dumps(keywords)+"\n")
	return keywords


class listener(StreamListener):
	def __init__(self, api=None):
		super(StreamListener, self).__init__()
		self.timestamp = int(time.time())+1000
		print (self.timestamp)

	def on_data(self, data):
		all_data = json.loads(data)
		opfile.write(str(all_data)+'\n')
		print (json.dumps(all_data,indent=4))
		if int(time.time())>self.timestamp:
			return (False)
		return(True)

	def on_error(self, status):
		return (True)

def main():
	auth = OAuthHandler(ckey, csecret)
	auth.set_access_token(atoken, asecret)
	auth_api = OAuthHandler(ckey, csecret)
	auth_api.set_access_token(atoken, asecret)
	api=API(auth_api)
	keywords=keys[:]
	tracking=track
	while True:
		# if True:
		try:		
			print (tracking)
			keywords=update_keywords(keywords,tracking,api)
			print (keywords)
			input()
			twitterStream = Stream(auth, listener())
			twitterStream.filter(track=keywords)
		except KeyboardInterrupt:
			opfile.close()
			raise
		except:
			pass
if __name__=='__main__':
	main()
