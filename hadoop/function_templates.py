#!/usr/bin/env python
import re, sys

text_pat=re.compile("u\'text\'\: u(.*?), u\'")
created_at_pat=re.compile("u\'created_at\'\: u(.*?), u\'")

def get_tweet_text(tweet):
    return text_pat.findall(tweet)[0]    

def get_created_at(tweet):
    return created_at_pat.findall(tweet)[-1] 

		
def main():
	for tweet in sys.stdin:
		try:
			'''
			SAMPLE CODE:
			'''
			t_text=get_tweet_text(tweet)
			t_created=get_created_at(tweet)
			print("Tweet text: %s\nCreated at: %s\n"%(t_text,t_created,))
			
		except:
			pass	
if __name__ == "__main__":
    main()
