#!/usr/bin/env python
import re,sys,string

text_pat=re.compile("u\'text\'\: u(.*?), u\'")
url_pat=re.compile("(?P<url>https?://[^\s]+)")
table = string.maketrans("","")
def get_tweet_text(tweet):
    return text_pat.findall(tweet)[0]    

def get_created_at(tweet):
    return created_at_pat.findall(tweet)[-1] 

		
def main():
	for tweet in sys.stdin:
		try:
			t_text=get_tweet_text(tweet)
			t_text=url_pat.sub("CUSTOM_URL",t_text)
			t_text=t_text.translate(table, "!\"#$%&'()*+,-./:;<=>?[]^`{|}~")
			t_text=t_text.replace("\\n","")
			for word in t_text.split():
				if not (word[0]=='@' or word=="CUSTOM_URL"):
					word=word.lower()
				elif word[0]=='@':
					print("USER_MENTION\t1");
				print ("%s\t1"%(word,))
		except:
			pass	
			
			
if __name__ == "__main__":
    main()
