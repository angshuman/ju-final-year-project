#!/usr/bin/env python
import re, sys

created_at_pat=re.compile("u\'created_at\'\: u(.*?), u\'")


def get_created_at(tweet):
    return created_at_pat.findall(tweet)[-1] 

		
def main():
	for tweet in sys.stdin:
		try:
			t_created=get_created_at(tweet)[1:17]
			print("%s\t1"%t_created)
			
		except:
			pass	
if __name__ == "__main__":
    main()
